<?php
defined('BASEPATH') or exit('No direct script access allowed');
//include controller master
include APPPATH . 'controllers/Master.php';

class admin extends Master
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Crud');
        // if(($this->session->userdata('login')!=true) || ($this->session->userdata('level')!=1) ){
        //     redirect(site_url('login/logout'));
        // }
        $this->cekadmin();
    }
    //VARIABEL
    private $master_tabel = "post"; //Mendefinisikan Nama Tabel
    private $id = "post_id"; //Menedefinisaikan Nama Id Tabel
    private $default_url = "post/admin/"; //Mendefinisikan url controller
    private $default_view = "post/admin/"; //Mendefinisiakn defaul view
    private $view = "template/backend"; //Mendefinisikan Tamplate Root
    private $path = './upload/featuredimage/';

    private function global_set($data)
    {
        $data = array(
            'menu' => 'berita', //Seting menu yang aktif
            'menu_submenu' => false,
            'submenu_menu' => 'post',
            'headline' => $data['headline'], //Deskripsi Menu
            'url' => $data['url'], //Deskripsi URL yang dilewatkan dari function
            'ikon' => "fa fa-tasks",
            'view' => "views/post/admin/index.php",
            'detail' => false,
            'cetak' => false,
            'edit' => true,
            'delete' => true,
            'download' => false,
            'aktifasi' => false,
        );
        return (object) $data; //MEMBUAT ARRAY DALAM BENTUK OBYEK
        //$data->menu=post, bentuk obyek
        //$data['menu']=post, array bentuk biasa
    }
    private function hapus_file($id)
    {
        $query = array(
            'tabel' => $this->master_tabel,
            'where' => array(array($this->id => $id)),
        );
        $file = $this->Crud->read($query)->row();
        if ($file->post_featuredimage) {
            unlink($this->path . $file->post_featuredimage);
        }
        //return $file->post_aktif;
    }
    public function index()
    {
        $global_set = array(
            'headline' => 'post',
            'url' => $this->default_url,
        );
        $global = $this->global_set($global_set);
        //CEK SUBMIT DATA
        if ($this->input->post('post_judul')) {
            //PROSES SIMPAN
            $data = array(
                'post_judul' => $this->input->post('post_judul'),
                'post_konten' => $this->input->post('post_konten'),
                'post_iduser' => $this->input->post('post_user'),
                'post_idkategori' => $this->input->post('post_idkategori'),
                'post_date' => date('Y-m-d', strtotime($this->input->post('post_date'))),
            );
            #############################################
            $file = 'fileupload';
            if ($_FILES[$file]['name']) {
                if ($this->gambarupload($this->path, $file)) {
                    $file = $this->upload->data('file_name');
                    $data['post_featuredimage'] = $file;
                } else {
                    $dt['error'] = $this->upload->display_errors();
                    return $this->output->set_output(json_encode($dt));
                    exit();
                }
            }
            $query = array(
                'data' => $data,
                'tabel' => $this->master_tabel,
            );
            $insert = $this->Crud->insert($query);
            if ($insert) {
                $dt['success'] = 'input data berhasil';
            } else {
                $dt['error'] = 'input data error';
            }
            return $this->output->set_output(json_encode($dt));
        } else {
            $data = array(
                'global' => $global,
                'menu' => $this->menu_backend($this->session->userdata('user_level')),
            );
            //$this->dumpdata($data);
            //$this->load->view($this->view,$data);
            $this->backend($data);
        }
    }
    public function tabel()
    {
        $global_set = array(
            'headline' => 'post',
            'url' => $this->default_url,
        );
        //LOAD FUNCTION GLOBAL SET
        $global = $this->global_set($global_set);
        //PROSES TAMPIL DATA
        // $query=array(
        //     'tabel'=>$this->master_tabel,
        //     'order'=>array('kolom'=>$this->id,'orderby'=>'DESC'),
        // );
        $join = array(
            'select' => 'a.*,b.kategori_nama,c.user_nama',
            'tabel' => 'post a',
            'join' => array(array('tabel' => 'kategori b', 'ON' => 'b.kategori_id=a.post_idkategori', 'jenis' => 'INNER'),
                array('tabel' => 'user c', 'ON' => 'c.user_id=a.post_iduser', 'jenis' => 'INNER'),
            ),
            'order' => array('kolom' => 'a.post_date', 'orderby' => 'DESC'),
        );
        $data = array(
            'global' => $global,
            'data' => $this->Crud->join($join)->result(),
        );
        $this->load->view($this->default_view . 'tabel', $data);
    }
    public function edit()
    {
        $global_set = array(
            'headline' => 'edit data',
            'url' => $this->default_url,
        );
        $global = $this->global_set($global_set);
        $id = $this->input->post('id');
        if ($this->input->post('post_judul')) {
            //PROSES SIMPAN
            $data = array(
                'post_judul' => $this->input->post('post_judul'),
                'post_konten' => $this->input->post('post_konten'),
                'post_iduser' => $this->input->post('post_user'),
                'post_idkategori' => $this->input->post('post_idkategori'),
                'post_date' => date('Y-m-d', strtotime($this->input->post('post_date'))),
            );
            ###################################################
            $file = 'fileupload';
            if ($_FILES[$file]['name']) {
                if ($this->gambarupload($this->path, $file)) {
                    $this->hapus_file($id);
                    $file = $this->upload->data('file_name');
                    $data['post_featuredimage'] = $file;
                } else {
                    $dt['error'] = $this->upload->display_errors();
                    return $this->output->set_output(json_encode($dt));
                    exit();
                }
            }
            $query = array(
                'data' => $data,
                'tabel' => $this->master_tabel,
                'where' => array($this->id => $id),
            );
            $update = $this->Crud->update($query);
            if ($update) {
                $dt['success'] = 'update data berhasil';
                $dt['id'] = $id;
            } else {
                $dt['error'] = 'input data error';
            }
            return $this->output->set_output(json_encode($dt));
        } else {
            $query = array(
                'tabel' => $this->master_tabel,
                'where' => array(array($this->id => $id)),
            );
            $kategori = array(
                'tabel' => 'kategori',
                'order' => array('kolom' => 'kategori_nama', 'orderby' => 'ASC'),
            );
            $kategori = $this->Crud->read($kategori)->result();
            $data = array(
                'data' => $this->Crud->read($query)->row(),
                'global' => $global,
                'kategori' => $kategori,
                'menu' => $this->menu(0),
            );
            //$this->viewdata($data);
            $this->load->view($this->default_view . 'edit', $data);
        }
    }
    public function add()
    {
        $global_set = array(
            'submenu' => false,
            'headline' => 'add data',
            'url' => $this->default_url, //AKAN DIREDIRECT KE INDEX
        );
        $global = $this->global_set($global_set);
        $kategori = array(
            'tabel' => 'kategori',
            'order' => array('kolom' => 'kategori_nama', 'orderby' => 'ASC'),
        );
        $kategori = $this->Crud->read($kategori)->result();
        $data = array(
            'global' => $global,
            'kategori' => $kategori,
        );
        $this->load->view($this->default_view . 'add', $data);
    }
    public function hapus()
    {
        $id = $this->input->post('id');
        $post = $this->hapus_file($id);
        $query = array(
            'tabel' => $this->master_tabel,
            'where' => array($this->id => $id),
        );
        $delete = $this->Crud->delete($query);
        if ($delete) {
            $dt['success'] = 'hapus data berhasil';
        } else {
            $dt['error'] = 'input data error';
            $dt['msg'] = $delete;
        }
        return $this->output->set_output(json_encode($dt));
    }
    public function download($file = null)
    {
        if ($file) {
            $this->downloadfile($this->path, $file);
        } else {
            $this->session->set_flashdata('error', 'File tidak ditemukan');
            redirect(site_url($this->default_url));
        }
    }

}
