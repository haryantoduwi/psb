<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
		// if(($this->session->userdata('login')!=true) || ($this->session->userdata('level')!=1) ){
		// 	redirect(site_url('login/logout'));
		// }
		$this->cekadmin();
	}
	//VARIABEL
	private $master_tabel="orangtua"; //Mendefinisikan Nama Tabel
	private $id="orangtua_id";	//Menedefinisaikan Nama Id Tabel
	private $default_url="orangtua/admin/"; //Mendefinisikan url controller
	private $default_view="orangtua/admin/"; //Mendefinisiakn defaul view
	private $view="template/backend"; //Mendefinisikan Tamplate Root
	private $path='./upload/fotopendaftar';

	private function global_set($data){
		$data=array(
			'menu'=>'pendaftaran',//Seting menu yang aktif
			'menu_submenu'=>false,
			'submenu_menu'=>'orang tua',
			'headline'=>$data['headline'], //Deskripsi Menu
			'url'=>$data['url'], //Deskripsi URL yang dilewatkan dari function
			'ikon'=>"fa fa-tasks",
			'view'=>"views/orangtua/admin/index.php",
			'detail'=>false,
			'add'=>false,
			'cetak'=>false,
			'edit'=>true,
			'delete'=>false,
			'download'=>false,
			'aktifasi'=>false,
		);
		return (object)$data; //MEMBUAT ARRAY DALAM BENTUK OBYEK
		//$data->menu=orangtua, bentuk obyek
		//$data['menu']=orangtua, array bentuk biasa
	}
	private function hapus_file($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->id=>$id)),
		);
		$file=$this->Crud->read($query)->row();
		if($file->orangtua_foto){
			unlink($this->path.$file->orangtua_foto);
		}
		//return $file->orangtua_aktif;
	}
	public function index()
	{
		$global_set=array(
			'headline'=>'orangtua',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);
		//CEK SUBMIT DATA
		if($this->input->post('orangtua_nama')){
			//PROSES SIMPAN
			$siswa=array(
				'orangtua_nama'=>$this->input->post('orangtua_nama'),
				'orangtua_tahunajaran'=>$this->input->post('orangtua_tahunajaran'),
				'orangtua_jeniskelamin'=>$this->input->post('orangtua_jeniskelamin'),
				'orangtua_asalsekolah'=>$this->input->post('orangtua_asalsekolah'),
				'orangtua_agama'=>$this->input->post('orangtua_agama'),
				'orangtua_tgllahir'=>date('Y-m-d',strtotime($this->input->post('orangtua_tgllahir'))),
				'orangtua_tempatlahir'=>$this->input->post('orangtua_tempatlahir'),
			);
			#############################################
			$file='foto';
			if($_FILES[$file]['name']){
				if($this->gambarupload($this->path,$file)){
					$file=$this->upload->data('file_name');
					$siswa['orangtua_foto']=$file;
					//print_r($data);
				}else{
					$dt['error']=$this->upload->display_errors();
					return $this->output->set_output(json_encode($dt));
					exit();
				}
			}			
			$query=array(
				'data'=>$siswa,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert_id($query);
			if($insert['param']){
				$orangtua=array(
					'orangtua_idorangtua'=>$insert['insert_id'],
					'orangtua_nama'=>$this->input->post('orangtua_nama'),
					'orangtua_nohp'=>$this->input->post('orangtua_notlp'),
					'orangtua_jeniskelamin'=>$this->input->post('orangtua_jeniskelamin'),
					'orangtua_alamat'=>$this->input->post('orangtua_alamat'),
				);
				$orangtua=array(
					'data'=>$orangtua,
					'tabel'=>'orangtua',
				);
				$insert2=$this->Crud->insert($orangtua);
				if($insert2){
					$dt['success']='input data berhasil';
				}else{
					$dt['error']='input data error';
				}
				return $this->output->set_output(json_encode($dt));				
			}else{
				$dt['error']='Simpan Calon Siswa Error';
				return $this->output->set_output(json_encode($dt));
				exit();				
			}

		}else{
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu_backend($this->session->userdata('user_level')),
			);
			//$this->dumpdata($data);			
			//$this->load->view($this->view,$data);
			$this->backend($data);
		}
	}
	public function tabel(){
		$global_set=array(
			'headline'=>'orangtua',
			'url'=>$this->default_url,
		);
		//LOAD FUNCTION GLOBAL SET
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'select'=>'a.*,b.calonsiswa_nama',
			'tabel'=>'orangtua a',
			'join'=>array(array('tabel'=>'calonsiswa b','ON'=>'b.calonsiswa_id=a.orangtua_idcalonsiswa','jenis'=>'INNER'),
				),
			'order'=>array('kolom'=>'a.orangtua_nama','orderby'=>'ASC'),
		);
		
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->join($query)->result(),
		);
		//$this->dumpdata($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}
	public function edit(){
		$global_set=array(
			'headline'=>'edit data',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('orangtua_nama')){
			//PROSES SIMPAN
			$data=array(
				'orangtua_nama'=>$this->input->post('orangtua_nama'),
				'orangtua_nohp'=>$this->input->post('orangtua_notlp'),
				'orangtua_jeniskelamin'=>$this->input->post('orangtua_jeniskelamin'),
				'orangtua_alamat'=>$this->input->post('orangtua_alamat'),
				
			);
			###################################################
			// $file='foto';
			// if($_FILES[$file]['name']){
			// 	if($this->gambarupload($this->path,$file)){
			// 		$this->hapus_file($id);
			// 		$file=$this->upload->data('file_name');
			// 		$data['orangtua_foto']=$file;
			// 	}else{
			// 		$dt['error']=$this->upload->display_errors();
			// 		return $this->output->set_output(json_encode($dt));
			// 		exit();	
			// 	}
			// }			
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
				'where'=>array($this->id=>$id),
			);
			$update=$this->Crud->update($query);
			if($update){
				$dt['success']='update data berhasil';
				$dt['id']=$id;
			}else{
				$dt['error']='input data error,msg '.$update;
			}
			return $this->output->set_output(json_encode($dt));			
		}else{
			$query=array(
				'select'=>'a.*,b.calonsiswa_nama',
				'tabel'=>'orangtua a',
				'join'=>array(array('tabel'=>'calonsiswa b','ON'=>'b.calonsiswa_id=a.orangtua_idcalonsiswa','jenis'=>'INNER'),
					),
				'where'=>array(array('orangtua_id'=>$id)),
			);			
			$data=array(
				'data'=>$this->Crud->join($query)->row(),
				'global'=>$global,
				'menu'=>$this->menu(0),
			);
			//$this->viewdata($data);			
			$this->load->view($this->default_view.'edit',$data);
		}			
	}	
	public function add(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'add data',
			'url'=>$this->default_url, //AKAN DIREDIRECT KE INDEX
		);	
		$global=$this->global_set($global_set);
		$tahunajaran=array(
			'tabel'=>'tahunajaran',
			'order'=>array('kolom'=>'tahunajaran_kode','orderby'=>'DESC'),
		);
		$agama=array(
			'tabel'=>'agama',
			'order'=>array('kolom'=>'agama_nama','orderby'=>'DESC'),
		);		
		$data=array(
			'global'=>$global,
			'tahunajaran'=>$this->Crud->read($tahunajaran)->result(),
			'agama'=>$this->Crud->read($agama)->result(),
			);
		//$this->dumpdata($data);
		$this->load->view($this->default_view.'add',$data);		
	}	
	public function hapus(){
		$id=$this->input->post('id');
		//$orangtua=$this->hapus_file($id);
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array($this->id=>$id),
		);
		$delete=$this->Crud->delete($query);
		if($delete){
			$dt['success']='hapus data berhasil';
		}else{
			$dt['error']='input data error';
			$dt['msg']=$delete;
		}
		return $this->output->set_output(json_encode($dt));	
	}
	public function download($file=null){
		if($file){
			$this->downloadfile($this->path,$file);
		}else{
			$this->session->set_flashdata('error','File tidak ditemukan');
			redirect(site_url($this->default_url));
		}
	}
	public function detail(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'detail orangtua',
			'url'=>$this->default_url, //AKAN DIREDIRECT KE INDEX
		);	
		$global=$this->global_set($global_set);		
		$id=$this->input->post('id');
		$query=array(
			'select'=>'a.*,b.agama_nama,c.tahunajaran_kode,d.*',
			'tabel'=>'orangtua a',
			'join'=>array(array('tabel'=>'agama b','ON'=>'b.agama_id=a.orangtua_agama','jenis'=>'INNER'),
				array('tabel'=>'tahunajaran c','ON'=>'c.tahunajaran_id=a.orangtua_tahunajaran','jenis'=>'INNER'),
				array('tabel'=>'orangtua d','ON'=>'d.orangtua_idorangtua=a.orangtua_id','jenis'=>'INNER'),
			),
			'where'=>array(array('a.orangtua_id'=>$id)),
		);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->join($query)->row(),
		);
		$this->load->view($this->default_view.'detail',$data);
	}
	// public function aktifasi(){
	// 	############### ALL SET 0
	// 	$prepare=array(
	// 		'orangtua_aktif'=>0,
	// 	);
	// 	$qprepare=array(
	// 		'tabel'=>$this->master_tabel,
	// 		'data'=>$prepare,
	// 	);
	// 	$prepare=$this->Crud->update($qprepare);
	// 	############### SET KE AKTIF
	// 	$id=$this->input->post('id');
	// 	$data=array(
	// 		'orangtua_aktif'=>1,
	// 	);
	// 	$query=array(
	// 		'tabel'=>$this->master_tabel,
	// 		'data'=>$data,
	// 		'where'=>array($this->id=>$id),
	// 	);
	// 	$update=$this->Crud->update($query);
	// 	if($update){
	// 		$dt['success']='Aktifasi sukses';
	// 	}else{
	// 		$dt['error']='Aktifasi gagal';
	// 	}
	// 	return $this->output->set_output(json_encode($dt));

	// }

}
