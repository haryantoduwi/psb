<div class="modal fade" id="modal-add" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-blue-active">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><?= ucwords($global->headline)?></h4>
			</div>
			<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url)?>"  enctype="multipart/form-data">			
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label>Tanggal</label>
							<input type="text" readonly name="kelas_tersimpan" class="form-control" value="<?= date('d-m-Y')?>">
						</div>							
						<div class="form-group">
							<label>Kode</label>
							<input required type="text" name="kelas_kode" class="text-capitalize form-control" title="Harus di isi">
						</div>
						<div class="form-group">
							<label>Nama</label>
							<input required type="text" name="kelas_nama" class="text-capitalize form-control" title="Harus di isi">
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<textarea required type="text" rows="4" class="form-control" name="kelas_keterangan"></textarea>
						</div>																									 
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12">						
						<div class="form-group">
							<button type="submit" value="submit" name="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
						</div>														
					</div>
				</div>
			</div>
			</form>	
		</div>
	</div>
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');
</script>
