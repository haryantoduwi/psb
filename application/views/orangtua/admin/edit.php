<div class="modal fade" id="modal-add"  role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-yellow-active">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><?= ucwords($global->headline)?></h4>
			</div>
			<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url.'edit')?>"  enctype="multipart/form-data">			
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label>Id</label>
							<input type="text" name="id" class="form-control" readonly="readonly" value="<?= $data->orangtua_id ?>" >
						</div>
						<div class="form-group">
							<label>Siswa</label>
							<input required type="text" readonly class="text-capitalize form-control" title="Harus di isi" value="<?= ucwords($data->calonsiswa_nama)?>">
						</div>						
						<div class="form-group">
							<label>Nama</label>
							<input required type="text" name="orangtua_nama" class="text-capitalize form-control" title="Harus di isi" value="<?= $data->orangtua_nama?>">
						</div>
						<div class="form-group">
							<label>No Telp</label>
							<input required type="text" name="orangtua_notlp" class="text-capitalize form-control" title="Harus di isi" value="<?= $data->orangtua_nohp?>">
						</div>							
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="radio">
							<label>
							  <input required type="radio" name="orangtua_jeniskelamin" id="laki" value="1" <?= $data->orangtua_jeniskelamin==1? 'checked':''?>>
								Laki-laki
							</label>
							<label style="margin-left: 20px">
							  <input required type="radio"  name="orangtua_jeniskelamin" id="perempuan" value="0" <?= $data->orangtua_jeniskelamin==0? 'checked':''?>>
							  	Perempuan
							</label>		                    
							</div>							
						</div>																	
						<div class="form-group">
							<label for="">Alamat</label>
							<textarea required class="form-control text-capitalize"  name="orangtua_alamat" rows="4"><?= $data->orangtua_alamat?></textarea>
						</div>		 
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12">						
						<div class="form-group">
							<button type="submit" value="submit" name="submit" class="btn btn-warning btn-block btn-flat">Update</button>
						</div>														
					</div>
				</div>
			</div>
			</form>	
		</div>
	</div>
</div>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');	
</script>
<?php include 'action.php'?>