<div class="row">
	<div class="col-sm-12">
		<h4 style="border-left: 5px solid #245571; padding-left: 10px">Daftar <?= ucwords($global->headline)?>
			<small>Silahkan pilih tahun ajaran dan kelas</small>
		</h4>
	</div>
</div>
<div class="row">
	<form>
		<div class="col-sm-2">
			<div class="form-group"> 
				<label>Tahun Ajaran</label>
				<select class="form-control selectdata" style="width:100%" >
					<option>Pilih Tahun Ajaran</option>
					<?php foreach($tahunajaran AS $row):?>
						<option value="<?= $row->tahunajaran_id?>"><?= $row->tahunajaran_kode?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<label>Kelas</label>
				<select class="form-control selectdata" style="width:100%" >
					<option>Pilih Kelas</option>
					<?php foreach($kelas AS $row):?>
						<option value="<?= $row->kelas_id?>"><?= $row->kelas_kode?></option>
					<?php endforeach;?>					
				</select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<label>&nbsp</label>
				<button type="submit" class="btn btn-flat btn-primary btn-block"><i class="fa fa-search"></i> Cari</button>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<label>&nbsp</label>
				<button type="button" class="btn btn-flat btn-warning btn-block"><i class="fa fa-print"></i> Cetak</button>
			</div>
		</div>		
	</form>
</div>
<br>
<div id="view">
	<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
		<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
	</div>
</div>
<div id="modal"></div>
<script type="text/javascript">
	$(document).ready(function(){
		var url=$('#tabel').attr('url');
		    setTimeout(function () {
	        $("#tabel").load(url);
	    }, 200); 		
	})
</script>
<?php include 'action.php';?>