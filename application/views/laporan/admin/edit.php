<div class="modal fade" id="modal-add"  role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-yellow-active">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><?= ucwords($global->headline)?></h4>
			</div>
			<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url.'edit')?>"  enctype="multipart/form-data">			
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label>Id</label>
							<input type="text" name="id" class="form-control" readonly="readonly" value="<?= $data->calonsiswa_id ?>" >
						</div>
						<div class="form-group">
							<label>Tahun Ajaran</label>
							<select required="required" name="calonsiswa_tahunajaran" class="form-control selectdata" style="width: 100%">
								<option selected="selected" disabled="disabled">Pilih Tahun Ajaran</option>
								<?php foreach($tahunajaran AS $row):?>
									<option value="<?= $row->tahunajaran_id?>" <?=$data->calonsiswa_tahunajaran==$row->tahunajaran_id ? 'Selected':''?>><?= ucwords($row->tahunajaran_kode)?></option>
								<?php endforeach;?>
							</select>
						</div>							
						<div class="form-group">
							<label>Nama</label>
							<input required type="text" name="calonsiswa_nama" class="text-capitalize form-control" title="Harus di isi" value="<?=$data->calonsiswa_nama?>">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="radio">
							<label>
							  <input type="radio" name="calonsiswa_jeniskelamin" id="laki" value="1" <?= $data->calonsiswa_jeniskelamin==1 ?'checked':''?>>
								Laki-laki
							</label>
							<label style="margin-left: 20px">
							  <input type="radio"  name="calonsiswa_jeniskelamin" id="perempuan" value="0" <?= $data->calonsiswa_jeniskelamin==0 ? 'checked':''?>>
							  	Perempuan
							</label>		                    
							</div>							
						</div>
						<div class="form-group">
							<label for="">Tempat Lahir</label>
							<input required name="calonsiswa_tempatlahir" class="form-control" value="<?= $data->calonsiswa_tempatlahir?>" />
						</div>												
						<div class="form-group">
							<label for="">Tanggal Lahir</label>
							<input required name="calonsiswa_tgllahir" class="datepicker form-control" value="<?= $data->calonsiswa_tgllahir?>" />
						</div>
						<div class="form-group">
							<label>Agama</label>
							<select required="required" name="calonsiswa_agama" class="form-control selectdata" style="width:100%">
								<?php foreach($agama AS $row):?>
									<option value="<?=$row->agama_id?>" <?= $data->calonsiswa_agama==$row->agama_id? 'selected':''?>><?= ucwords($row->agama_nama)?></option>
								<?php endforeach;?>
							</select>
						</div>							
						<div class="form-group">
							<label for="">Asal Sekolah</label>
							<textarea required class="form-control"  name="calonsiswa_asalsekolah" rows="4"><?= $data->calonsiswa_asalsekolah?></textarea>
						</div>
						<div class="form-group">
							<label>Foto</label>
							<input type="file" name="foto">
							<p class="help-block">Ukuran max 5mb, jpeg|jpg</p>
						</div>	 
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12">						
						<div class="form-group">
							<button type="submit" value="submit" name="submit" class="btn btn-warning btn-block btn-flat">Update</button>
						</div>														
					</div>
				</div>
			</div>
			</form>	
		</div>
	</div>
</div>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');	
</script>
<?php include 'action.php'?>