<div class="row">
	<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url.'edit')?>"  enctype="multipart/form-data">
		<div class="col-sm-8">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Tulis Berita</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="">Judul</label>
						<input required type="text" class="text-capitalized form-control" name="post_judul" value="<?=$data->post_judul?>"/>
					</div>
					<div class="form-group">
						<label for="">Konten</label>
						<textarea required name="post_konten" rows="10" class="form-control"><?=$data->post_konten?></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 animated bounceInRight">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?= ucwords($global->headline)?></h3>
					<button type="button" onclick="loaddata()" class="btn btn-xs pull-right btn-danger btn-flat"><i class="fa fa-arrow-left"></i> Kembali</button>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Tanggal</label>
								<input type="text" required name="post_date" class="datepicker form-control" value="<?=date('d-m-Y',strtotime($data->post_date))?>">
							</div>
							<div class="form-group hide">
								<label>User</label>
								<input type="text" readonly name="id" class="form-control" value="<?= $data->post_id?>">
							</div>
							<div class="form-group hide">
								<label>User</label>
								<input type="text" readonly name="post_user" class="form-control" value="<?= $this->session->userdata('user_id')?>">
							</div>														
							<div class="form-group">
								<label>Kategori</label>
								<select required type="text" name="post_idkategori" class="form-control selectdata" style="width:100%" title="Harus di isi">
									<option selected="selected" disabled value="">Pilih Kategori</option>
									<?php foreach($kategori AS $row):?>
										<option value="<?=$row->kategori_id?>" <?= $data->post_idkategori==$row->kategori_id? 'selected':''?>><?= ucwords($row->kategori_nama)?></option>
									<?php endforeach;?>
								</select>
							</div>
							<div class="form-group">
								<label>Featuredimage</label>
								<input type="file" name="fileupload"></input>
							</div>
							<div class="form-group">
								<button class="btn btn-flat btn-block btn-warning">Update</button>
							</div>																		 
						</div>
					</div>	
				</div>
			</div>		
		</div>
	</form>		
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');
</script>
