<div class="modal fade" id="modal-add" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-blue-active">
	            <button type="button" class="pull-right btn btn-box-tool" data-widget="collapse" data-dismiss="modal" aria-label="Close" ><i class="fa fa-close" style="color: white"></i>
	            </button>				
				<h4 class="modal-title"><?= ucwords($global->headline)?></h4>
			</div>
			<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url)?>"  enctype="multipart/form-data">			
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-9"> 
						<table class="table table-striped" width="100%">
							<tr>
								<th colspan="2" class="bg-blue" style="color: white">Data Diri</th>
							</tr>
							<tr>
								<th width="20%">Nama</th>
								<td width="80%" align="left"><?= ucwords($data->calonsiswa_nama)?></td>
							</tr>
							<tr>
								<th>Tahun Ajaran</th>
								<td align="left"><?= $data->tahunajaran_kode?></td>
							</tr>							
							<tr>
								<th>Jenis Kelamin</th>
								<td align="left"><?= $data->calonsiswa_jeniskelamin==1? 'Laki-laki':'Perempuan'?></td>
							</tr>
							<tr>
								<th>Agama</th>
								<td align="left"><?= ucwords($data->agama_nama)?></td>
							</tr>
							<tr>
								<th>Tgl.Lahir</th>
								<td align="left"><?=ucwords($data->calonsiswa_tempatlahir).', '.date('d-m-Y',strtotime($data->calonsiswa_tgllahir))?></td>
							</tr>
							<tr>
								<th>Asal Sekolah</th>
								<td align="left"><?= ucwords($data->calonsiswa_asalsekolah)?></td>
							</tr>	
							<tr>
								<th colspan="2" class="bg-blue-active" style="color: white">Wali Murid</th>
							</tr>
							<tr>
								<th>Wali/Orang Tua</th>
								<td align="left"><?= ucwords($data->orangtua_nama)?></td>
							</tr>
							<tr>
								<th>Telephon</th>
								<td align="left"><?= ucwords($data->orangtua_nohp)?></td>
							</tr>
							<tr>
								<th>Alamat</th>
								<td align="left"><?= ucwords($data->orangtua_alamat)?></td>
							</tr>																					
						</table>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<?php if($data->calonsiswa_foto):?>
								<img src="<?= base_url('upload/fotopendaftar/'.$data->calonsiswa_foto)?>" style="width: 100%;height: 240px">
							<?php else:?>
								<img src="<?= base_url('upload/fotopendaftar/avatar.png')?>" style="width: 100%;height: 240px">
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12">						
						<div class="form-group">
							<button type="button" data-dismiss="modal" class="btn btn-primary btn-block btn-flat">Tutup</button>
						</div>														
					</div>
				</div>
			</div>
			</form>	
		</div>
	</div>
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');
</script>