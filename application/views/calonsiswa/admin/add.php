<style type="text/css">
	.nav-tabs-custom .nav .nav-tabs li.active a,
	 .nav-tabs-custom .nav-tabs li a{
		background-color: #357CA5;
		color: white;
	}
</style>
<div class="modal fade" id="modal-add" role="dialog">
	<div class="modal-dialog" style="max-width: 1200px;width: 100%">
		<div class="modal-content">
			<div class="modal-header bg-blue-active">
	            <button type="button" class="pull-right btn btn-box-tool" data-widget="collapse" data-dismiss="modal" aria-label="Close" ><i class="fa fa-close" style="color: white"></i>
	            </button>				
				<h4 class="modal-title"><?= ucwords($global->headline)?></h4>
			</div>
			<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url)?>"  enctype="multipart/form-data">			
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<legend class="bg-orange-active" style="color: white;padding: 10px"><span class="fa  fa-user"></span> Data Siswa</legend>
						<div class="form-group">
							<label>Tahun Ajaran</label>
							<select required="required" name="calonsiswa_tahunajaran" class="form-control selectdata" style="width: 100%">
								<option selected="selected" disabled="disabled">Pilih Tahun Ajaran</option>
								<?php foreach($tahunajaran AS $row):?>
									<option value="<?= $row->tahunajaran_id?>"><?= ucwords($row->tahunajaran_kode)?></option>
								<?php endforeach;?>
							</select>
						</div>							
						<div class="form-group">
							<label>Nama</label>
							<input required type="text" name="calonsiswa_nama" class="text-capitalize form-control" title="Harus di isi">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="radio">
							<label>
							  <input type="radio" name="calonsiswa_jeniskelamin" id="laki" value="1">
								Laki-laki
							</label>
							<label style="margin-left: 20px">
							  <input type="radio"  name="calonsiswa_jeniskelamin" id="perempuan" value="0">
							  	Perempuan
							</label>		                    
							</div>							
						</div>
						<div class="form-group">
							<label for="">Tempat Lahir</label>
							<input required name="calonsiswa_tempatlahir" class="form-control"/>
						</div>												
						<div class="form-group">
							<label for="">Tanggal Lahir</label>
							<input required name="calonsiswa_tgllahir" class="datepicker form-control"/>
						</div>
						<div class="form-group">
							<label>Agama</label>
							<select required="required" name="calonsiswa_agama" class="form-control selectdata" style="width: 100%">
								<option  selected="selected" disabled="disabled">Pilih Tahun Agama</option>
								<?php foreach($agama AS $row):?>
									<option value="<?=$row->agama_id?>"><?= ucwords($row->agama_nama)?></option>
								<?php endforeach;?>
							</select>
						</div>							
						<div class="form-group">
							<label for="">Asal Sekolah</label>
							<textarea required class="form-control"  name="calonsiswa_asalsekolah" rows="4"></textarea>
						</div>
						<div class="form-group">
							<label>Foto</label>
							<input type="file" name="foto">
							<p class="help-block">Ukuran max 5mb, jpeg|jpg</p>
						</div>						
					</div>
					<div class="col-sm-6">		
						<legend class="bg-blue" style="color: white;padding: 10px"><span class="fa  fa-users"></span> Data Orang Tua</legend>
						<div class="form-group">
							<label>Nama</label>
							<input required type="text" name="orangtua_nama" class="text-capitalize form-control" title="Harus di isi">
						</div>
						<div class="form-group">
							<label>No Telp</label>
							<input required type="text" name="orangtua_notlp" class="text-capitalize form-control" title="Harus di isi">
						</div>							
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="radio">
							<label>
							  <input required type="radio" name="orangtua_jeniskelamin" id="laki" value="1">
								Laki-laki
							</label>
							<label style="margin-left: 20px">
							  <input required type="radio"  name="orangtua_jeniskelamin" id="perempuan" value="0">
							  	Perempuan
							</label>		                    
							</div>							
						</div>																	
						<div class="form-group">
							<label for="">Alamat</label>
							<textarea required class="form-control"  name="orangtua_alamat" rows="4"></textarea>
						</div>	

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12">						
						<div class="form-group">
							<button type="submit" value="submit" name="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
						</div>														
					</div>
				</div>
			</div>
			</form>	
		</div>
	</div>
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');
</script>
