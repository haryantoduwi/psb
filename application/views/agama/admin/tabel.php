<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary  animated bounceInDown">
		        <div class="box-header bg-blue">
		          <h3 class="box-title"><span class="fa fa-table"></span> Tabel <?php echo ucwords($global->headline)?></h3>
		        </div>
		        <div class="box-body table-responsive">
		        	<table style="width:100%" id="datatabel" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  	<th width="5%">No</th>
			                  	<th width="85%">Agama</th>
			                  	<th width="10%" class="text-center">Aksi</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php $i=1;foreach ($data as $row):?>
			                	<tr>
			                		<td><?=$i?></td>
			                		<td><?=ucwords($row->agama_nama)?></td>
			                		<td class="text-center">
			                			<?php include 'button.php';?>
			                		</td>
			                	</tr>	                	
		                	<?php $i++;endforeach;?>
		                </tbody>            		
		        	</table>
		        	<p><span class="fa fa-question"></span> Keterangan : <br>
		        		<span class="<?= !$global->edit? 'hide':''?>">
		        		<a href="#" class="btn btn-flat btn-xs btn-info" style="width:25px"><span class="fa fa-pencil"></span></a> : Edit<br>
		        		</span>
		        		<span class="<?= !$global->delete? 'hide':''?>">
		        		<a href="#" class="btn btn-flat btn-xs btn-danger" style="width:25px"><span class="fa fa-trash"></span></a> : Hapus	
		        		</span>
		        	</p>
		        </div>
		</div>		
	</div>
</div>
<?php include 'action.php'; ?>