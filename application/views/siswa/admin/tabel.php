<div class="row">
	<div class="col-sm-12">
		<div class="box box-primary  animated bounceInDown">
		        <div class="box-header">
		          <h3 class="box-title">Tabel <?php echo ucwords($global->headline)?></h3>
		        </div>
		        <div class="box-body table-responsive">
		        	<table style="width:100%" id="datatabel" class="table table-bordered table-striped">
		                <thead>
			                <tr>
			                  <th width="5%">No</th>
			                  <th width="5%">Tahun Ajaran</th>
			                  <th width="20%">Nama</th>
			                  <th width="10%">Agama</th>
			                  <th width="20%">Tanggal Lahir</th>
			                  <th width="10%">Jenis Kelamin</th>
							  <th width="10%">Asal Sekolah</th>
			                  <th width="10%" class="text-center">Aksi</th>
			                </tr>
		                </thead>
		                <tbody>
		                	<?php $i=1;foreach ($data as $row):?>
			                	<tr>
			                		<td><?=$i?></td>
			                		<td><?=$row->tahunajaran_kode?></td>
			                		<td><?=ucwords($row->calonsiswa_nama)?></td>	
			                		<td><?=ucwords($row->agama_nama)?></td>
			                		<td><?=ucwords($row->calonsiswa_tempatlahir).', '.date('d-m-Y',strtotime($row->calonsiswa_tgllahir))?></td>
			                		<td><?= $row->calonsiswa_jeniskelamin==1 ? 'Laki-laki':'Perempuan'?></td>
									<td><?=ucwords($row->calonsiswa_asalsekolah)?></td>
			                		<td class="text-center">
			                			<?php include 'button.php';?>
			                		</td>
			                	</tr>	                	
		                	<?php $i++;endforeach;?>
		                </tbody>            		
		        	</table>
		        	<p>Keterengan : <br>
		        		<a href="#" class="btn btn-flat btn-xs btn-info" style="width:25px"><span class="fa fa-pencil"></span></a> : Edit<br>
		        		<a href="#" class="btn btn-flat btn-xs btn-danger" style="width:25px"><span class="fa fa-trash"></span></a> : Hapus	
		        	</p>
		        </div>
		</div>		
	</div>
</div>
<?php include 'action.php'; ?>