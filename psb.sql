/*
 Navicat Premium Data Transfer

 Source Server         : PHPMYADMIN
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : psb

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 10/01/2019 06:24:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for agama
-- ----------------------------
DROP TABLE IF EXISTS `agama`;
CREATE TABLE `agama`  (
  `agama_id` int(11) NOT NULL AUTO_INCREMENT,
  `agama_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `agama_status` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`agama_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of agama
-- ----------------------------
INSERT INTO `agama` VALUES (1, 'islam', b'1');
INSERT INTO `agama` VALUES (2, 'kristen', b'1');
INSERT INTO `agama` VALUES (3, 'katolik', b'1');
INSERT INTO `agama` VALUES (4, 'budha', b'1');
INSERT INTO `agama` VALUES (5, 'hindu', b'1');

-- ----------------------------
-- Table structure for calonsiswa
-- ----------------------------
DROP TABLE IF EXISTS `calonsiswa`;
CREATE TABLE `calonsiswa`  (
  `calonsiswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `calonsiswa_tahunajaran` int(255) NULL DEFAULT NULL,
  `calonsiswa_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `calonsiswa_jeniskelamin` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `calonsiswa_tgllahir` date NULL DEFAULT NULL,
  `calonsiswa_agama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `calonsiswa_asalsekolah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `calonsiswa_tempatlahir` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `calonsiswa_foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`calonsiswa_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of calonsiswa
-- ----------------------------
INSERT INTO `calonsiswa` VALUES (7, 1, 'markuest', '1', '2019-01-08', '2', 'sd 1 kayangana', 'bantul', '492a978542c8934532ab06388bcc37cb.jpg');
INSERT INTO `calonsiswa` VALUES (8, 3, 'duwi haryantoa', '1', '2011-08-12', '1', 'SD N 1 Wijirejo\r\n', 'bantul', '751ab3e347be9391efc71faadc1292a4.jpeg');

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas`  (
  `kelas_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_kode` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelas_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelas_keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kelas_tersimpan` date NULL DEFAULT NULL,
  `kelas_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`kelas_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kelas
-- ----------------------------
INSERT INTO `kelas` VALUES (1, 'IA', 'Kelas satu a', '-', '2019-01-06', NULL);
INSERT INTO `kelas` VALUES (2, '1B', 'kelas satu b', '-\r\n', '2019-01-06', NULL);
INSERT INTO `kelas` VALUES (3, '1C', 'kelas satu c', '-', '2019-01-06', NULL);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_ikon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_is_mainmenu` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_akses_level` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_urutan` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_status` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_akses` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (5, 'download', 'fa fa-download', '0', 'frontend/download', '0', '3', '0', 0);
INSERT INTO `menu` VALUES (6, 'kegiatan', 'fa fa-tasks', '0', 'frontend/kegiatan', '0', '2', '0', 0);
INSERT INTO `menu` VALUES (7, 'kegiatan', 'fa  fa-calendar-check-o ', '0', 'kegiatan/admin', '1', '1', '0', 1);
INSERT INTO `menu` VALUES (8, 'bukutamu', 'fa fa-book', '0', 'bukutamu/admin', '1', '1', '0', 1);
INSERT INTO `menu` VALUES (9, 'user', 'fa fa-circle-o', '13', 'user/admin', '1', '3', '1', 1);
INSERT INTO `menu` VALUES (10, 'bukutamu', 'fa fa-book', '0', 'frontend/bukutamu', '0', '1', '1', 0);
INSERT INTO `menu` VALUES (11, 'login', 'fa fa-lock', '0', 'login', '0', '1', '1', 1);
INSERT INTO `menu` VALUES (12, 'pendaftaran', 'fa fa-users', '0', 'pendaftaran/admin', '1', '1', '1', 1);
INSERT INTO `menu` VALUES (13, 'setting', 'fa fa-gears', '0', 'setting/admin', '1', '99', '1', 1);
INSERT INTO `menu` VALUES (14, 'tentang', 'fa fa-question', '13', 'tentang/admin', '1', '99', '1', 1);
INSERT INTO `menu` VALUES (15, 'siswa', 'fa fa-circle-o', '12', 'calonsiswa/admin', '1', '1', '1', 1);
INSERT INTO `menu` VALUES (16, 'orang tua', 'fa fa-circle-o', '12', 'orangtua/admin', '1', '1', '1', 1);
INSERT INTO `menu` VALUES (17, 'tahun ajaran', 'fa fa-circle-o', '13', 'tahunajaran/admin', '1', '1', '1', 1);
INSERT INTO `menu` VALUES (18, 'kelas', 'fa fa-circle-o', '13', 'kelas/admin', '1', '2', '1', 1);
INSERT INTO `menu` VALUES (19, 'pekerjaan', 'fa fa-circle-o', '13', 'pekerjaan/admin', '1', '5', '1', 1);
INSERT INTO `menu` VALUES (20, 'agama', 'fa fa-circle-o', '13', 'agama/admin', '1', '6', '1', 1);
INSERT INTO `menu` VALUES (21, 'siswa', 'fa fa-users', '0', 'siswa/admin', '1', '2', '1', 1);
INSERT INTO `menu` VALUES (22, 'laporan', 'fa fa-newspaper-o', '0', 'laporan/admin', '1', '4', '1', 1);

-- ----------------------------
-- Table structure for orangtua
-- ----------------------------
DROP TABLE IF EXISTS `orangtua`;
CREATE TABLE `orangtua`  (
  `orangtua_id` int(11) NOT NULL AUTO_INCREMENT,
  `orangtua_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `orangtua_nohp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `orangtua_alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `orangtua_jeniskelamin` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `orangtua_idcalonsiswa` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`orangtua_id`) USING BTREE,
  INDEX `fk_idcalonsiswa_on_orangtua`(`orangtua_idcalonsiswa`) USING BTREE,
  CONSTRAINT `fk_idcalonsiswa_on_orangtua` FOREIGN KEY (`orangtua_idcalonsiswa`) REFERENCES `calonsiswa` (`calonsiswa_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orangtua
-- ----------------------------
INSERT INTO `orangtua` VALUES (1, 'pedrosa', '08573434', 'bantul', '1', 7);
INSERT INTO `orangtua` VALUES (2, 'murdiyana', '085734343', 'Pedak wijirejo pandak bantul', '1', 8);

-- ----------------------------
-- Table structure for pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `pekerjaan`;
CREATE TABLE `pekerjaan`  (
  `pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pekerjaan_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pekerjaan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pekerjaan
-- ----------------------------
INSERT INTO `pekerjaan` VALUES (1, 'Buruh');
INSERT INTO `pekerjaan` VALUES (2, 'PNS');
INSERT INTO `pekerjaan` VALUES (4, 'TNI/POLRI');
INSERT INTO `pekerjaan` VALUES (5, 'Petani');

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa`  (
  `siswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_nis` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `siswa_idcalonsiswa` int(255) NULL DEFAULT NULL,
  `siswa_terdaftar` date NULL DEFAULT NULL,
  PRIMARY KEY (`siswa_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tahunajaran
-- ----------------------------
DROP TABLE IF EXISTS `tahunajaran`;
CREATE TABLE `tahunajaran`  (
  `tahunajaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tahunajaran_kode` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahunajaran_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahunajaran_tersimpan` date NULL DEFAULT NULL,
  `tahunajaran_update` date NULL DEFAULT NULL,
  PRIMARY KEY (`tahunajaran_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tahunajaran
-- ----------------------------
INSERT INTO `tahunajaran` VALUES (1, '2019/2010', 'Genap', '2019-01-05', NULL);
INSERT INTO `tahunajaran` VALUES (2, '2018/2019', 'ganjil', '2019-01-05', NULL);
INSERT INTO `tahunajaran` VALUES (3, '2017/2018', 'genap', '2019-01-06', NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `user_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_level` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_terdaftar` date NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 'admina', '1', '2018-09-29');
INSERT INTO `user` VALUES (3, 'haryanto', 'haryanto', 'haryanto duwi', '2', '2018-10-21');
INSERT INTO `user` VALUES (4, 'duwi', 'duwi', 'duwi', '1', '2018-11-30');
INSERT INTO `user` VALUES (5, 'panitia01', 'panitia01', 'panitai01', '1', '2019-01-05');

SET FOREIGN_KEY_CHECKS = 1;
